package br.com.publitech.cadastro.persistencia.api;

import javax.ejb.Local;

import br.com.publitech.cadastro.orm.Cliente;

@Local
public interface ClienteDao extends GenericDao<Cliente>{
}
