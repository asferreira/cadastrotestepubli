package br.com.publitech.cadastro.persistencia.api;

import java.util.List;

public interface GenericDao<T>{
	
	public void add(T entityClass);
	
	public void update(T entityClass);
	
	public void delete(long id);
	
	public T get(long id);
	
	public List<T> get();
	
}
