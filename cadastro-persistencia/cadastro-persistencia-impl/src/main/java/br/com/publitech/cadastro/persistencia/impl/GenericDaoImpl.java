package br.com.publitech.cadastro.persistencia.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.publitech.cadastro.persistencia.api.GenericDao;

public abstract class GenericDaoImpl<T> implements GenericDao<T> {

	@PersistenceContext
	protected EntityManager entityManager;

	public Class<T> entityClass;

	public Class<T> getEntityClass() {
		return entityClass;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public GenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		entityClass = ((Class) pt.getActualTypeArguments()[0]);
	}

	public void add(T object) {
		entityManager.persist(object);
	}

	public void update(T object) {
		entityManager.merge(object);
	}

	public void delete(long id) {
		entityManager.remove(entityManager.getReference(entityClass, id));
	}

	public T get(long id) {
		return entityManager.find(entityClass, id);
	}
	
	public List<T> get(){
		
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(entityClass);
		Root<T> root = criteria.from(entityClass);
		
		criteria.select(root);
		
		try {
			return entityManager.createQuery(criteria).getResultList();
		} catch (NoResultException e) {	
			return null;
		}
	}

}
