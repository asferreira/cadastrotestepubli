package br.com.publitech.cadastro.persistencia.impl;

import br.com.publitech.cadastro.persistencia.api.ClienteDao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.publitech.cadastro.orm.Cliente;

@Stateless
public class ClienteDaoImpl extends GenericDaoImpl<Cliente> implements ClienteDao{	
	
	@Override
	public List<Cliente> get(){
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Cliente> criteria = builder.createQuery(entityClass);
		Root<Cliente> root = criteria.from(entityClass);
		
		criteria.select(root).distinct(true);
		
		try {
			return entityManager.createQuery(criteria).getResultList();
		} catch (NoResultException e) {	
			return null;
		}
		
	}
}
