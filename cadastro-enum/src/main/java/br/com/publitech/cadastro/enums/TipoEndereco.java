package br.com.publitech.cadastro.enums;

public enum TipoEndereco {
	PRINCIPAL(0), COBRANCA(1), COMERCIAL(2), ENTREGA(3);
	
	private final int valor;
    
	TipoEndereco(int tipoPessoa){
        valor = tipoPessoa;
    }
    
	public int getValor(){
        return valor;
    }
}
