package br.com.publitech.cadastro.enums;

public enum TipoRegimeTributario {
	ISENTO(0), SIMPLES_NACIONAL(1), LUCRO_PRESUMIDO(2), LUCRO_REAL(3);
	
	private final int valor;
    
	TipoRegimeTributario(int tipoRegime){
        valor = tipoRegime;
    }
    
	public int getValor(){
        return valor;
    }
	
}
