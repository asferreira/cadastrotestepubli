package br.com.publitech.cadastro.enums;

public enum TipoPessoa {
	PF(0), PJ(1);
	
	private final int valor;
    
	TipoPessoa(int tipoPessoa){
        valor = tipoPessoa;
    }
    
	public int getValor(){
        return valor;
    }
	
}
