package br.com.publitech.cadastro.controle.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.publitech.cadastro.controle.api.ClienteController;
import br.com.publitech.cadastro.persistencia.api.ClienteDao;
import br.com.publitech.cadastro.persistencia.api.GenericDao;
import br.com.publitech.cadastro.orm.*;
import br.com.publitech.cadastro.controle.impl.util.*;

@Stateless
public class ClienteControllerImpl extends GenericControllerImpl<Cliente> implements ClienteController {

	@SuppressWarnings("cdi-ambiguous-dependency")
	@Inject
	private ClienteDao clienteDao;

	@Override
	protected GenericDao<Cliente> getModelDaoApi() {
		// TODO Auto-generated method stub
		return clienteDao;
	}

	@Override
	public void add(Cliente entityClass) {
		if (Util.validaCliente(entityClass)) {
			entityClass.getEnderecos().forEach(endereco -> endereco.setCliente(entityClass));
			super.add(entityClass);
		}else {
			throw new RuntimeException();
		}
	}

	@Override
	public void update(Cliente entityClass) {
		if (Util.validaCliente(entityClass)) {
			entityClass.getEnderecos().forEach(endereco -> endereco.setCliente(entityClass));
			super.update(entityClass);
		}else {
			throw new RuntimeException();
		}
	}

	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		super.delete(id);
	}

	@Override
	public Cliente get(long id) {
		// TODO Auto-generated method stub
		return super.get(id);
	}

	@Override
	public List<Cliente> get() {
		return super.get();
	}

	public static boolean validaEnderecos(List<Endereco> enderecos) {
		boolean valido = false;

		return valido;
	}

}
