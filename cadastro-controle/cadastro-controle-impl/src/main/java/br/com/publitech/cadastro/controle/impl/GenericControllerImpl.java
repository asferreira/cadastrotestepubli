package br.com.publitech.cadastro.controle.impl;



import java.util.List;

import br.com.publitech.cadastro.controle.api.GenericController;
import br.com.publitech.cadastro.persistencia.api.GenericDao;

public abstract class GenericControllerImpl<T> implements GenericController<T>{
	
		
	protected abstract GenericDao<T> getModelDaoApi();
	
	public void add(T entityClass) {
		// TODO Auto-generated method stub
		getModelDaoApi().add(entityClass);
	}

	public void update(T entityClass) {
		// TODO Auto-generated method stub
		getModelDaoApi().update(entityClass);
	}

	public void delete(long id) {
		// TODO Auto-generated method stub
		getModelDaoApi().delete(id);
	}

	public T get(long id) {
		// TODO Auto-generated method stub
		return getModelDaoApi().get(id);
	}
	
	public List<T> get() {
		return getModelDaoApi().get();
	}

}
