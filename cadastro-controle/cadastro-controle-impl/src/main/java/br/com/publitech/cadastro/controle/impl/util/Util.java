package br.com.publitech.cadastro.controle.impl.util;

import br.com.publitech.cadastro.enums.TipoEndereco;
import br.com.publitech.cadastro.enums.TipoPessoa;
import br.com.publitech.cadastro.orm.Cliente;
import br.com.publitech.cadastro.orm.Endereco;

public class Util {
	
	public static boolean validaCliente(Cliente cliente) {
		boolean valido = true;
		
		if (cliente.getTipo().equals(TipoPessoa.PF)) {
			cliente.setCnpj(null);
			cliente.setRegimeTrib(null);
			// se a pessoa e fisica verifica CPF
			if (cliente.getCpf() == null) {
				valido = false;
			}
		} else {
			cliente.setCpf(null);
			//se a pessoa e juridica verifica cnpj
			if (cliente.getCnpj() == null) {
				valido = false;
			} else {
				//verifica regime tributario
				if (cliente.getRegimeTrib() == null) {
					valido = false; // apenas por enquanto
				}
			}
		}
		if (!cliente.getEnderecos().isEmpty()) {
			int count = 0;
			for (Endereco e : cliente.getEnderecos()) {
				if (e.getTipo().equals(TipoEndereco.PRINCIPAL)) {
					count++;
				}
			}
			if(count == 0){
				valido = false; // nenhum endereco principal
			}
			else if (count == 2) {
				valido = false; //mais de um endereco principal
			}

		}else {
			valido = false; //nenhum  endereco
		}

		return valido;
	}

}
