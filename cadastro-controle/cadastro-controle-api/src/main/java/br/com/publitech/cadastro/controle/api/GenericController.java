package br.com.publitech.cadastro.controle.api;

import java.util.List;

import javax.ejb.Local;


public interface GenericController <T> {

	public void add(T entityClass);
	
	public void update(T entityClass);
	
	public void delete(long id);
	
	public T get(long id);
	
	public List<T> get();
}
