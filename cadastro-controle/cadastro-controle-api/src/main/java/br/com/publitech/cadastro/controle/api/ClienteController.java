package br.com.publitech.cadastro.controle.api;

import javax.ejb.Local;

import br.com.publitech.cadastro.orm.Cliente;

@Local
public interface ClienteController extends GenericController<Cliente>{
	
}
