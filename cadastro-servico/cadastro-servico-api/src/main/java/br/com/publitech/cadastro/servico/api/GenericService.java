package br.com.publitech.cadastro.servico.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public interface GenericService<T> {
	
	@GET	//m�todo http
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON) //forma de comunica��o JSON
	@Path("/{id}") //caminho do parametro
	public T  get (@PathParam("id") long id);
	
	@GET	//m�todo http
	@Produces(MediaType.APPLICATION_JSON)
	public List<T>  get();
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void add(T entityClass);
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void update(T entityClass);
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public void delete(@PathParam("id") long id);
	
	
}
