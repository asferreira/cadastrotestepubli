package br.com.publitech.cadastro.servico.api;

import javax.ws.rs.Path;

import br.com.publitech.cadastro.orm.Cliente;

@Path("/cliente")
public interface ClienteService extends GenericService<Cliente>{

}
