package br.com.publitech.cadastro.servico.impl;

import java.util.List;

import br.com.publitech.cadastro.controle.api.GenericController;
import br.com.publitech.cadastro.servico.api.GenericService;

public abstract  class GenericServiceImpl<T> implements GenericService<T>{
	
	//private Generic persistencia;
	protected abstract GenericController<T> getControllerApi();
	
	@Override
	public T get(long id) {
		return getControllerApi().get(id);
	}

	@Override
	public List<T> get() {
		return getControllerApi().get();
	}

	@Override
	public void add(T entityClass) {
		getControllerApi().add(entityClass);
		
	}

	@Override
	public void update(T entityClass) {
		getControllerApi().update(entityClass);
		
	}

	@Override
	public void delete(long id) {
		getControllerApi().delete(id);
	}

	
}
