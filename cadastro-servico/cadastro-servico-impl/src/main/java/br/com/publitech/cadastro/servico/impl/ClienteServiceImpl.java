package br.com.publitech.cadastro.servico.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.publitech.cadastro.controle.api.ClienteController;
import br.com.publitech.cadastro.controle.api.GenericController;
import br.com.publitech.cadastro.orm.Cliente;
import br.com.publitech.cadastro.servico.api.ClienteService;

@Stateless
public class ClienteServiceImpl extends GenericServiceImpl<Cliente> implements ClienteService{

	@Inject
	private ClienteController controller;
	
	@Override
	protected GenericController<Cliente> getControllerApi() {
		// TODO Auto-generated method stub
		return controller;
	}
	

}
