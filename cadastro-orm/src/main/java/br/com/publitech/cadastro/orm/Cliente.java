/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.publitech.cadastro.orm;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.publitech.cadastro.enums.*;

/**
 *
 * @author alex_
 */
@Entity
@Table(name="clientes")
public class Cliente implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="CUSTOMER_ID", precision=0)
    private Long id;
    @Column(length=100, nullable=false)
    private String nome;
    @Enumerated(EnumType.ORDINAL)
    @Column(length=10, nullable=false)
    private TipoPessoa tipo;
    @Column(nullable=false, updatable=false)
    private Date dataCadastro;
    @CPF
    @Column(length=11, unique=true)
    private String cpf;
    @CNPJ
    @Column(length=14, unique=true)
    private String cnpj;
	@Column(length=20)
    private TipoRegimeTributario regimeTrib;
    
	@JsonManagedReference
    @OneToMany(mappedBy="cliente", targetEntity = Endereco.class, 
    		cascade = CascadeType.ALL,  orphanRemoval = true, fetch=FetchType.EAGER)
    private List<Endereco> enderecos;

   public List<Endereco> getEnderecos() {
       if(enderecos == null) {
    	   enderecos = new ArrayList<Endereco>();
       }
	   return enderecos;
   }

    public void setEnderecos(List<Endereco> enderecos) {
      this.enderecos = enderecos;
   }
    

    public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    
    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public TipoRegimeTributario getRegimeTrib() {
        return regimeTrib;
    }

    public void setRegimeTrib(TipoRegimeTributario regimeTrib) {
        this.regimeTrib = regimeTrib;
    }
    

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoPessoa getTipo() {
        return tipo;
    }

    public void setTipo(TipoPessoa tipoPessoa) {
        this.tipo = tipoPessoa;
    }
    

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cadastro.Cliente[ id=" + id + " "+ nome + " ]";
    }
    
}
