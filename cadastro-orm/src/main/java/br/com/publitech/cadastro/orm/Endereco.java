/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.publitech.cadastro.orm;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.publitech.cadastro.enums.TipoEndereco;

/**
 *
 * @author alex_
 */

@Entity
@Table(name="enderecos")
public class Endereco implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ENDERECO_ID", precision=0)
    private Long id;
    
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="CUSTOMER_ID")
    private Cliente cliente;
    
    @Column(name="cep", length=8, nullable=false)
    private String cep;
    
    @Column(name="cidade", length=60, nullable=false)
    private String cidade;
    
    @Column(name="logradouro", length=60, nullable=false)
    private String logradouro;
    
    @Column(name="numero", length=10)
    private String numero;
    
    @Column(name="bairro", length=30, nullable=false)
    private String bairro;
    
    @Column(name="complemento", length=30)
    private String complemento;
    
    @Column(name="tipo", length=20, nullable=false)
    private TipoEndereco tipo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public TipoEndereco getTipo() {
        return tipo;
    }

    public void setTipo(TipoEndereco tipo) {
        this.tipo = tipo;
    }
    
    
    
    
}
